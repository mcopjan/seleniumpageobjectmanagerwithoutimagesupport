﻿using System;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Xml.Linq;
using Microsoft.Win32;
using TestAutomation.UI.Elements;
using static SeleniumPageObjectsManager.Utility;


namespace SeleniumPageObjectsManager
{
    public partial class MainWindow : Window
    {
        public static ObservableCollection<Element> _elements;
        private string _xmlDirectoryPath;
        private string _xmlPath;
        private string _xmlFileName;

        public MainWindow()
        {
            InitializeComponent();
            TextBoxLocator.IsEnabled = false;
            ButtonAddPageObject.IsEnabled = false;
        }

        private void LoadElementsFromXml()
        {
            _elements = new ObservableCollection<Element>();
            XDocument xdoc = XDocument.Load(_xmlPath);
            var nodes = xdoc.Descendants("Children").ToList().First().Descendants("Children").First().Nodes().ToList();
            var allAvailableTypes = Assembly
                .GetExecutingAssembly()
                .GetReferencedAssemblies()
                .Select(x => Assembly.Load(x))
                .SelectMany(x => x.GetTypes()).ToList();
            foreach (var node in nodes)
            {
                if (node is XComment)
                {
                    imageWarningIcon.Visibility = Visibility.Visible;
                    imageWarningIcon.ToolTip = "There are some commented elements in the xml.";
                }
                else
                {
                    var xElement = ((XElement)node);

                    var type =
                        allAvailableTypes.First(t => t.FullName == $"TestAutomation.UI.Elements.{xElement.Name.LocalName}");


                    var element = (Element)Activator.CreateInstance(type);
                    element.Name = xElement.Descendants("Name").First().Value;
                    element.By = xElement.Elements().Last().Name.ToString();
                    element.Locator = xElement.Elements().Last().Value;
                    _elements.Add(element);
                }

            }

            ListOfPageObjects.ItemsSource = _elements;
            ComboBoxElementType.ItemsSource = GetAllElementTypes<Element>();
            ComboBoxBy.ItemsSource = GetListOfAllByMethods();
            SelectFirstElementInComboBox(ComboBoxElementType);
            SelectFirstElementInComboBox(ComboBoxBy);
        }


        private void btnAddPageObject_Click(object sender, RoutedEventArgs e)
        {
            if (TextBoxLocator.Text == string.Empty)
            {
                MessageBox.Show("Locator textBox is empty!", "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
                ButtonAddPageObject.IsEnabled = false;
            }
            else
            {
                var type = ComboBoxElementType.SelectionBoxItem.GetType();
                var o = (Element)Activator.CreateInstance(type);
                o.Name = TextBoxElementName.Text;
                o.Locator = TextBoxLocator.Text;
                o.By = ComboBoxBy.SelectedValue.ToString();
                _elements.Add(o);
                AddElementToXml(o);
                TextBoxElementName.Text = string.Empty;
                TextBoxLocator.Text = string.Empty;
            }
        }



        private void AddElementToXml(Element element)
        {
            XDocument doc = XDocument.Load(_xmlPath);
            XElement root = new XElement(element.Type);

            root.Add(new XElement(nameof(element.Name), element.Name));
            root.Add(new XElement(element.By, element.Locator));
            doc.Descendants("Children").ToList().Last().LastNode.AddAfterSelf(root);
            doc.Save(_xmlPath);
        }

        private void PageObject_DoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (ListOfPageObjects.SelectedItems.Count != 0)
            {
                var element = (Element)ListOfPageObjects.SelectedItems[0];
                ListOfPageObjects.SelectedItems.Clear();
                XDocument xdoc = XDocument.Load(_xmlPath);
                var rootElement = xdoc.Descendants("Name").First(el => el.Value == element.Name).Parent;
                var imagePath = string.Empty;
                if (rootElement.HasAttributes)
                {
                    imagePath = rootElement.Attribute("ImagePath")?.Value;
                }


                ElementDetailsWindow window = new ElementDetailsWindow(_xmlDirectoryPath, _xmlPath, element.Name,
                    element.By, element.Locator, element.Type,
                    imagePath);
                window.ShowDialog();
                LoadElementsFromXml();
            }
        }


        private void btnSelectXml_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                OpenFileDialog openFileDialog = new OpenFileDialog
                {
                    Multiselect = false,
                    Filter = "Xml files (*.xml)|*.xml"
                };
                if (openFileDialog.ShowDialog() == true)
                    _xmlPath = openFileDialog.FileName;
                TextBoxXmlPath.Text = openFileDialog.FileName;
                if (_xmlPath != null)
                {
                    FileInfo fileInfo = new FileInfo(_xmlPath);
                    _xmlDirectoryPath = fileInfo.Directory.FullName;
                    _xmlFileName = fileInfo.Name.Replace(".xml", "");
                    LoadElementsFromXml();

                    TextBoxXmlPath.ToolTip = _xmlPath;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show($"An error encountered when loadind xml -> {ex.Message}", "Error!", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void TextBoxElementName_OnTextChanged(object sender, TextChangedEventArgs e)
        {
            if (_elements != null)
            {
                if (_elements.Any(l => l.Name == TextBoxElementName.Text) && TextBoxElementName.Text != string.Empty)
                {
                    ButtonAddPageObject.IsEnabled = false;
                    TextBoxElementName.Background = new SolidColorBrush(Colors.OrangeRed);
                    TextBoxElementName.ToolTip = "An element with the same name already exists!";
                    TextBoxLocator.IsEnabled = false;
                }
                else if (TextBoxElementName.Text == string.Empty)
                {
                    ButtonAddPageObject.IsEnabled = false;
                }
                else
                {
                    TextBoxLocator.IsEnabled = true;
                    ButtonAddPageObject.IsEnabled = true;
                    TextBoxElementName.ToolTip = "";
                    TextBoxElementName.Background = new SolidColorBrush(Colors.White);
                }
            }
        }
    }
}
