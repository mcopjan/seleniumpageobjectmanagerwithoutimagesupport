﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SeleniumPageObjectsManager
{
    public class TestObject
    {
        public string Name { get; set; }
        public bool IsHiglighted { get; set; }
    }
}
