﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Controls;

namespace SeleniumPageObjectsManager
{
    public static class Utility
    {
        public static List<string> GetListOfAllByMethods()
        {
            return new List<string>()
            {
                "ClassName",
                "CssSelector",
                "Id",
                "LinkText",
                "Name",
                "PartialLinkText",
                "TagName",
                "XPath"
            };
        }

        public static void SelectElementInComboBox(ComboBox comboBox, int position)
        {
            comboBox.SelectedIndex = position;
        }

        public static void SelectFirstElementInComboBox(ComboBox comboBox)
        {
            SelectElementInComboBox(comboBox, 0);
        }

        public static IEnumerable<T> GetAllElementTypes<T>()
        {
            return typeof(T)
                .Assembly.GetTypes()
                .Where(t => t.IsSubclassOf(typeof(T)) && !t.IsAbstract)
                .Select(t => (T)Activator.CreateInstance(t)).ToList();
        }
    }
}
