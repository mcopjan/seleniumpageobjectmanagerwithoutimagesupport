﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SeleniumPageObjectsManager
{
    /// <summary>
    /// Interaction logic for ConverterTestWindow.xaml
    /// </summary>
    public partial class ConverterTestWindow : Window
    {
        public ObservableCollection<TestObject> AllItems { get; set; }

        public ConverterTestWindow()
        {
            InitializeComponent();
            AllItems = new ObservableCollection<TestObject>();

            AllItems.Add(new TestObject { Name = "Martin", IsHiglighted = false });
            AllItems.Add(new TestObject { Name = "Rohan", IsHiglighted = true });
            //listBoxTest.ItemsSource = AllItems;


        }
    }
}
