﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SeleniumPageObjectsManager
{
    /// <summary>
    /// Interaction logic for Window2.xaml
    /// </summary>
    public partial class Window2 : Window
    {
        System.Collections.ObjectModel.ObservableCollection<MyItem> items = new System.Collections.ObjectModel.ObservableCollection<MyItem>();
        public Window2()
        {
            InitializeComponent();
            
            items.Add(new MyItem { Title = "MyItem1", Completion = 50 });
            items.Add(new MyItem { Title = "MyItem2", Completion = 10 });
            items.Add(new MyItem { Title = "MyItem3", Completion = 90 });
            listItems.ItemsSource = items;

            

        }

        class MyItem
        {
            public string Title { get; set; }
            public int Completion { get; set; }
        }

        private void clickButton(object sender, RoutedEventArgs e)
        {
            for (int i = 0; i < 7; i++)
            {
                items[1].Completion += 10;
                Thread.Sleep(1000);
            }
        }
    }
}
