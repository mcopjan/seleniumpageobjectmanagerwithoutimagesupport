﻿using System.Linq;
using System.Windows;
using System.Xml.Linq;
using TestAutomation.UI.Elements;

namespace SeleniumPageObjectsManager
{
    /// <summary>
    /// Interaction logic for ElementDetailsWindow.xaml
    /// </summary>
    public partial class ElementDetailsWindow : Window
    {
        private readonly string _name;
        private readonly string _by;
        private readonly string _locator;
        private readonly string _type;
        private string XmlDirectory;
        private string XmlFilePath;

        public ElementDetailsWindow(string xmlDirectory, string xmlFilePath, string name, string by, string locator,
            string type,
            string imagePath)
        {
            XmlDirectory = xmlDirectory;
            XmlFilePath = xmlFilePath;
            InitializeComponent();
            comboBoxBy.ItemsSource = Utility.GetListOfAllByMethods();
            comboBoxType.ItemsSource = Utility.GetAllElementTypes<Element>();

            _by = by;
            _name = name;
            _locator = locator;
            _type = type;

            comboBoxBy.SelectedValue = _by;
            textBoxLocator.Text = _locator;
            textBoxName.Text = _name;
            comboBoxType.SelectedItem = comboBoxType.Items.OfType<Element>().ToList().First(i => i.Type == _type);
        }


        private void button_Click(object sender, RoutedEventArgs e)
        {
            XDocument doc = XDocument.Load(XmlFilePath);
            var allElements = doc.Descendants("Children").ToList().Last();
            var elementToEdit = doc.Descendants(_type).First(d => d.Element("Name").Value == _name);
            elementToEdit.Name = comboBoxType.SelectedValue.ToString();
            elementToEdit.Element(_by).Name = comboBoxBy.SelectedValue.ToString();
            elementToEdit.Element(comboBoxBy.SelectedValue.ToString()).Value = textBoxLocator.Text;
           
            //add validation here
            doc.Save(XmlFilePath);
            Close();
        }


        private void buttonDelete_Click(object sender, RoutedEventArgs e)
        {
            XDocument doc = XDocument.Load(XmlFilePath);
            var allElements = doc.Descendants("Children").ToList().Last();
            
            doc.Descendants(_type).First(d => d.Element("Name").Value == _name).Remove();
            doc.Save(XmlFilePath);
            Close();
        }
    }
}
