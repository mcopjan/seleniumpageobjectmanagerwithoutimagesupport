﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SeleniumPageObjectsManager
{
    /// <summary>
    /// Interaction logic for Window3.xaml
    /// </summary>
    public partial class Window3 : Window
    {
        public CollectionViewSource ViewSource { get; set; }
        private ObservableCollection<Person> persons = new ObservableCollection<Person>();

        public Window3()
        {
            InitializeComponent();
            persons.Add(new Person { FirstName = "TestFirstName", LastName = "TestLastName" });
            dgPerson.ItemsSource = persons;
            persons.CollectionChanged += Persons_CollectionChanged;

        }

        private void Persons_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            ObservableCollection<Person> obsSender = sender as ObservableCollection<Person>;
            var action = e.Action;
            if (action == NotifyCollectionChangedAction.Add)
                lblStatus.Content = "New person added";
            if (action == NotifyCollectionChangedAction.Remove)
                lblStatus.Content = "Person deleted";
        }

        





        public class Person : INotifyPropertyChanged
        {
            private string lastName;
            private string firstName;


            public string LastName
            {
                get { return lastName; }
                set
                {
                    lastName = value;
                    RaisePropertyChanged("LastName");
                }
            }

            public string FirstName
            {
                get { return firstName; }
                set
                {
                    firstName = value;
                    RaisePropertyChanged("FirstName");
                }
            }

            public event PropertyChangedEventHandler PropertyChanged;
            private void RaisePropertyChanged(string propertyName)
            {
                if (PropertyChanged != null)
                {
                    PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
                }
            }
        }

        private void btnAdd_Click(object sender, RoutedEventArgs e)
        {
            Person p = new Person();
            p.PropertyChanged += P_PropertyChanged;
            p.FirstName = txtFname.Text;
            p.LastName = txtLname.Text;
            persons.Add(p);
        }

        private void P_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            //save changes to xml, already done in UI
        }

        private void btnRemove_Click(object sender, RoutedEventArgs e)
        {
            var person = persons.First(p => p.FirstName == txtFname.Text && p.LastName == txtLname.Text);
            persons.Remove(person);
        }

        private void OnDataGridCellEditEnding(object sender, DataGridCellEditEndingEventArgs e)
        {
           //var changedValue = ((TextBox)e.EditingElement).Text;
            //persons
        }
    }
}
